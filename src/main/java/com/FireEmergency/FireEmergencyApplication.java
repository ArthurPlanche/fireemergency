package com.FireEmergency;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class FireEmergencyApplication {

	public static void main(String[] args) {
		SpringApplication.run(FireEmergencyApplication.class, args);
	}

}
