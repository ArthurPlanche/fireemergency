package com.FireEmergency.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.FireEmergency.Model.FireDTO;
import com.FireEmergency.Model.Fire;
import com.FireEmergency.Model.ConfigDTO;

import com.FireEmergency.Repository.RepoFire;

@Service
public class ServiceFeu {
	
	@Autowired
	RepoFire fRepository;
	@Autowired
	ServiceAffectation ServiceA;
	
	String url_docker = "http://localhost:8081";
	
	public ServiceFeu () {
		
	}
	
	public List<Fire> getFireList () {
		Iterable<Fire> listeFireIterable = fRepository.findAll();
		List<Fire> listeFire = new ArrayList<Fire>();
		listeFireIterable.forEach(listeFire::add);
		return listeFire;
	}
	
	public Fire getFireByIdSim (int id) {
		Optional<Fire> f = fRepository.findByIdSim(id);
		if (f.isPresent()) {
			return f.get();
		}else {
			return null;
		}
	}
	
	public Fire getFireById (int id) {
		Optional<Fire> f = fRepository.findById(id);
		if (f.isPresent()) {
			return f.get();
		}else {
			return null;
		}
	}
	public void updateFireRepo () {
		// On met a jour notre repo en fonction des feux dans le docker
		RestTemplate restTemplate = new RestTemplate();
		FireDTO[] ret = restTemplate.getForObject(url_docker + "/fire", FireDTO[].class);
		System.out.println("Update FIRE");
		
		List<Integer> listId = new ArrayList<>();
		
		for (FireDTO f : ret) {
			Optional<Fire> fRepo = fRepository.findByIdSim(f.getId());
			//Création des nouveaux feux
			if (!fRepo.isPresent()) {
				Fire fSave = f.fromDTO();
				fRepository.save(fSave);
				ServiceA.affection(fSave);
			}
			//Maj des feux déjà présent
			else {	
				Fire fMaj = this.getFireByIdSim(f.getId());
				fMaj.setRange(f.getRange());
				fMaj.setIntensity(f.getIntensity());
				fRepository.save(fMaj);
			}
			listId.add(f.getId());
			
		}
		
		//Supression des feux éteints
		Iterable<Fire> listeFireIterable = fRepository.findAll();
		List<Fire> listeFire = new ArrayList<Fire>();
		listeFireIterable.forEach(listeFire::add);
		for (Fire fire : listeFire) {
			int idf = fire.getIdSim();
			if (!listId.contains(idf)) {
				fRepository.delete(fire);
			}	
		}
	    return;    
	}
	
	//Modifie la configuration de création des feux
	public void changeFeuConfig (ConfigDTO config) {
		
		System.out.println(config);//affiche la probabilité
		
		//put la requête sur le docker
		RestTemplate restTemplate = new RestTemplate();
		ConfigDTO updateInfo = config;
		HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<ConfigDTO> requestBody = new HttpEntity<>(updateInfo, headers);
		restTemplate.put(url_docker + "/config/creation" , requestBody, new Object[] {});		
	}
	
	public void resetFeu() {
		//On fait le reset sur le docker
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getForEntity(url_docker + "/fire/reset",null, new Object[] {});
		//On vide notre repository
		fRepository.deleteAll();
	
	}
	
	
	//recupération de la config de création des feux
	public ConfigDTO getConfig() {
		
		RestTemplate restTemplate = new RestTemplate();
		ConfigDTO conf = restTemplate.getForObject(url_docker + "/config/creation",ConfigDTO.class);
		
		
		return conf;
	
	}
	
	//permet de changer l'état du feu si il est pris en charge par un véhicule ou non
	public void ChangeAffectation(int idFeu,boolean bool) {
		Fire f = getFireById(idFeu);
		if (f != null) {
			f.setAffectation(bool);
			fRepository.save(f);
			//System.out.println("fire:"+idFeu+" affectation:"+bool);
		}
		
	}
	
	
}


