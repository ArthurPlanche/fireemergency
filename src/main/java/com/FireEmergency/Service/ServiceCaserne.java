package com.FireEmergency.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.FireEmergency.Model.Caserne;
import com.FireEmergency.Model.Deplacement;
import com.FireEmergency.Model.Vehicule;
import com.FireEmergency.Repository.RepoCaserne;

@Service
public class ServiceCaserne {
	
	@Autowired
	RepoCaserne cRepository;
	
	public Caserne newCaserne (double lon, double lat, int capacity) {
		Caserne c = new Caserne(lon, lat, capacity);
		cRepository.save(c);
		return c;
	}
	
	public List<Caserne> getListCaserne() {
		Iterable<Caserne> listeCaserneIterable = cRepository.findAll();
		List<Caserne> listeCaserne = new ArrayList<Caserne>();
		listeCaserneIterable.forEach(listeCaserne::add);
		return listeCaserne;
	}
	
	public void changeCaserne(int id, Caserne c) {
		Caserne cRepo = getCaserne(id);
		if (cRepo != null) {
			cRepo = c;
		}
		cRepository.save(cRepo);
	}
	
	public void delCaserne(int id) {
		Caserne c = getCaserne(id);
		if (c != null) {
			cRepository.delete(c);
		}
	}
	
	public Caserne getCaserne (int id) {
		Optional<Caserne> c = cRepository.findById(id);
		if (c.isPresent()) {
			return c.get();
		}else {
			return null;
		}
	}
	
	public void updateCasernes() {
		
	}
	
}
