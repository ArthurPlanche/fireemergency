package com.FireEmergency.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.FireEmergency.Repository.RepoDeplacement;
import com.FireEmergency.Repository.RepoFire;
import com.FireEmergency.Repository.RepoVehicule;

@Service
public class UpdateService {
	
	private int sleepTime = 700;
	
	private ServiceVehicule vService;
	private ServiceDeplacement dService;
	private ServiceFeu fService;
	private ServiceAffectation aService;
	
	DisplayRunnable dRunnable;
	DisplayRunnable2 dRunnable2;
	private Thread displayThread;
	private Thread displayThread2;

	public UpdateService (ServiceVehicule v, ServiceDeplacement d, ServiceFeu f, ServiceAffectation a) {
		
		//Replace the @Autowire annotation....
		this.vService = v;
		this.dService = d;
		this.aService = a;
		this.fService = f;
		
		//Create a Runnable is charge of executing cyclic actions 
		this.dRunnable=new DisplayRunnable(this.vService, this.dService, this.fService, this.aService, sleepTime);
		this.dRunnable2=new DisplayRunnable2(this.vService, this.dService, this.fService, this.aService, sleepTime);
		
		// A Runnable is held by a Thread which manage lifecycle of the Runnable
		displayThread=new Thread(dRunnable);
		displayThread2=new Thread(dRunnable2);
		
		// The Thread is started and the method run() of the associated DisplayRunnable is launch
		vService.updateVehiculeRepo();
		displayThread.start();
		displayThread2.start();

	}
	
	public void stopDisplay() {
		//Call the user defined stop method of the runnable
		this.dRunnable.stop();
		this.dRunnable2.stop();
		try {
			//force the thread to stop
			this.displayThread.join(100);
			this.displayThread2.join(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	
}
