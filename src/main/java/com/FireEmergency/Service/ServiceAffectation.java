package com.FireEmergency.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.awt.geom.Point2D;

import com.FireEmergency.Model.Caserne;
import com.FireEmergency.Model.EtatVehicule;
import com.FireEmergency.Model.Fire;
import com.FireEmergency.Model.Vehicule;
import com.FireEmergency.Model.mapBoxDTO;



@Service
public class ServiceAffectation {
	
	@Autowired
    ServiceVehicule ServiceV;
	@Autowired
    ServiceFeu ServiceF;
	
	@Autowired
    ServiceDeplacement ServiceD;
	@Autowired
    ServiceCaserne ServiceC;
	
	//utile pour les requêtes mapbox
	String url_docker = "http://localhost:8081";
	String url_mapbox = "https://api.mapbox.com/directions/v5/mapbox/driving/";
	String mapboxSettings = "?geometries=geojson&access_token=";
	String mapboxKey = "pk.eyJ1IjoidGhlbWRlaWwiLCJhIjoiY2twZTBuMXBzMXNzNjJubnhkdjU2YWVnaSJ9.JQOr5slR2vCCmEUEWNjI2A";
	
	public ServiceAffectation () {
		
	}
	
	//Calcul distance entre deux points départ lonV/latV et arrivée lonF/latF 
	public float calculDistance(double lonF,double latF,double lonV,double latV) {
		
		
		 String url_request = url_mapbox + String.valueOf(lonV) + "," + String.valueOf(latV)
			 + ";" + String.valueOf(lonF) + "," + String.valueOf(latF)
			 + mapboxSettings + mapboxKey;
		 
		 RestTemplate restTemplate = new RestTemplate(); 
		 mapBoxDTO m = restTemplate.getForObject(url_request, mapBoxDTO.class);
		 float duration = m.getRoutes().get(0).getDuration();
		 return duration;
	 }
	
	//calcul de la consommation de fuel 
	public float calculFuelConsumption(double lonF,double latF,double lonV,double latV,float fuelCons) {
		
		 String url_request = url_mapbox + String.valueOf(lonV) + "," + String.valueOf(latV)
			 + ";" + String.valueOf(lonF) + "," + String.valueOf(latF)
			 + mapboxSettings + mapboxKey;
		 
		 RestTemplate restTemplate = new RestTemplate(); 
		 mapBoxDTO m = restTemplate.getForObject(url_request, mapBoxDTO.class);
		 float distance = m.getRoutes().get(0).getDistance();
		 return (distance/1000)*fuelCons;
		 
	 }
	
	//Calcul distance entre un feu et un vehicule ligne droite (plus utilisée)
	 public float calculDist(double lonF,double latF,double lonV,double latV) {
		    
		  RestTemplate restTemplate = new RestTemplate();   
		  float distance = restTemplate.getForObject(url_docker +"/fire/distance?lonCoord1="+lonF+"&latCoord1="+latF+"&lonCoord2="+lonV+"&latCoord2="+latV, float.class);
		  
		  return distance; 
	}
	 
  
	  
	  //Affectation d'un feu au véhiculele plus proche
	 public void affection (Fire f){ 
		 //Affiche les caractéristique du feu
		 System.out.println(f); 
		 //Coord du feu 
		 double lonF = f.getLon(); 
		 double latF = f.getLat();
	  
		  //Récupération des véhicules
		  List<Vehicule> listV = ServiceV.getVehiculeList();
		  
		  //variables initiales
		  float distance = 99999999; 
		  int idV=0;
		  float consoFuel=0;
		  
		  for(Vehicule v : listV) { 
			  
			  
			  //condition vehicule disponible
			  if (v.getEtat()== EtatVehicule.DISPONIBLE) {
				  double lonV=v.getLon(); 
				  double latV=v.getLat();
				  float fuelCons = v.getFuelConsumption();
				  float consumption = calculFuelConsumption(lonF,latF,lonV,latV,fuelCons);
				  float consumptionTotal= consumption;
				  if (v.getFacilityRefID()!=0) {
					  Caserne c = ServiceC.getCaserne(v.getFacilityRefID());
					  float consumption2 = calculFuelConsumption(lonF,latF,c.getLon(),c.getLat(),fuelCons);
					  consumptionTotal += consumption2;
				  }
				  //condition assez de carburant
				  if( consumptionTotal<v.getFuel()) {
				  
					  float newD = this.calculDistance(lonF,latF,lonV,latV); 
					  if (newD<distance) { 
						  distance=newD; 
						  idV=v.getId();
						  consoFuel=consumption;
					  }
				  }
			  }
		  }
	  
	  	  //Si on a trouvé un vehicule
		  if (idV != 0) {
			  System.out.println("Affectation "+idV+" in "+distance);
			  
			  Vehicule vAffect = ServiceV.getVehicule(idV);
	
			  //On ajoute un idFeu au Vehicule et le 
			  ServiceV.setidFeu(vAffect.getId(),f.getIdSim());
			  ServiceV.changeEtat(vAffect.getId(),EtatVehicule.DIRFEU);
			  
			  //déclenche un déplacement
			  ServiceD.genItineraire(vAffect.getId(), lonF, latF,consoFuel);
			  
			  ServiceF.ChangeAffectation(f.getId(),true);
			  
		  }
		  else {
			  System.out.println("Pas de vehicule disponible");
			  
		  }
	 }
	 
	 
	 public void updateAffectation (){
		 List<Fire> listFeu = ServiceF.getFireList();
		 for (Fire f : listFeu) {
			 if (f.getAffectation() == false) {
				 this.affection(f);
			 }
		 }
	 }
}
