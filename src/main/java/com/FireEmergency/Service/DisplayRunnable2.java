package com.FireEmergency.Service;

import org.springframework.beans.factory.annotation.Autowired;

public class DisplayRunnable2 implements Runnable {

	private ServiceVehicule vService;
	private ServiceDeplacement dService;
	private ServiceFeu fService;
	private ServiceAffectation aService;
	private int sleepTime;
	boolean isEnd = false;
	
	public DisplayRunnable2 (ServiceVehicule v, ServiceDeplacement d, ServiceFeu f, ServiceAffectation a, int sleepTime) {
		this.vService = v;
		this.dService = d;
		this.aService = a;
		this.fService = f;
		this.sleepTime = sleepTime;
	}

	@Override
	public void run() {
		while (!this.isEnd) {
			try {
				System.out.println("Update2 Running....");
				Thread.sleep(sleepTime);
				dService.updateDeplacement();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("Runnable DisplayRunnable ends.... ");
	}

	public void stop() {
		this.isEnd = true;
	}

}
