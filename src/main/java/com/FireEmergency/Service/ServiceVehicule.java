package com.FireEmergency.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import com.FireEmergency.Model.VehiculeDTO;
import com.FireEmergency.Model.mapBoxDTO;
import com.FireEmergency.Model.Caserne;
import com.FireEmergency.Model.EtatVehicule;
import com.FireEmergency.Model.Vehicule;
import com.FireEmergency.Repository.RepoVehicule;


@Service
public class ServiceVehicule {
	
	@Autowired
	RepoVehicule vRepository;
	
	@Autowired
	ServiceFeu fService;
	
	@Autowired
	ServiceAffectation aService;
	
	@Autowired
	ServiceCaserne cService;
	
	@Autowired
	ServiceDeplacement dService;

	String url_docker = "http://localhost:8081";
	
	public ServiceVehicule () {
		
	}
	
	public Vehicule getVehicule (int id) {
		Optional<Vehicule> v = vRepository.findById(id);
		if (v.isPresent()) {
			return v.get();
		}else {
			return null;
		}
	}
	
	public Vehicule getVehiculeByIdSim (int id) {
		Optional<Vehicule> v = vRepository.findByIdSim(id);
		if (v.isPresent()) {
			return v.get();
		}else {
			return null;
		}
	}
	
	public List<Vehicule> getVehiculeList () {
		this.updateVehiculeRepo();
		Iterable<Vehicule> listeVehiculeIterable = vRepository.findAll();
		List<Vehicule> listeVehicule = new ArrayList<Vehicule>();
		listeVehiculeIterable.forEach(listeVehicule::add);
		return listeVehicule;
	}
	
	public void delVehicule (int id) {
		Vehicule v = getVehicule(id);
		int idSim = v.getIdSim();
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.delete(url_docker + "/vehicle/"+ String.valueOf(idSim), boolean.class);
		vRepository.delete(v);
	}
	
	public void changeVehiculeInfo (int id) {
		// Changer les infos du vehicule dans le docker
		Vehicule v = getVehicule(id);
		if (v != null) {
			VehiculeDTO vDTO = v.toDTO();
			RestTemplate restTemplate = new RestTemplate();
			VehiculeDTO updateInfo = vDTO;
			HttpHeaders headers = new HttpHeaders();
	        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
			HttpEntity<VehiculeDTO> requestBody = new HttpEntity<>(updateInfo, headers);
			restTemplate.put(url_docker + "/vehicle/" + String.valueOf(v.getIdSim()), requestBody, new Object[] {});
		}
	}
	
	public void changeVehiculeFromDTO (int id, VehiculeDTO vDTO) {
		Vehicule v = getVehicule(id);
		if (v != null) {
			vDTO.setId(v.getIdSim());
			v.setLon(vDTO.getLon());
			v.setLat(vDTO.getLat());
			v.setType(vDTO.getType());
			v.setLiquidType(vDTO.getLiquidType());
			v.setFacilityRefID(vDTO.getFacilityRefID());
			v.setFuel(vDTO.getFuel());
			//v.setLiquidQuantity(vDTO.getLiquidQuantity());
			//v.setFuelConsumption(vDTO.getFuelConsumption());
			RestTemplate restTemplate = new RestTemplate();
			VehiculeDTO updateInfo = vDTO;
			HttpHeaders headers = new HttpHeaders();
	        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
			HttpEntity<VehiculeDTO> requestBody = new HttpEntity<>(updateInfo, headers);
			restTemplate.put(url_docker + "/vehicle/" + String.valueOf(v.getIdSim()), requestBody, new Object[] {});
			vRepository.save(v);
		}
	}
	
	public void moveVehicule (int id, double lon, double lat) {
		Vehicule v = getVehicule(id);
		if (v != null) {
			v.setLon(lon);
			v.setLat(lat);
			vRepository.save(v);
			changeVehiculeInfo(id);
			System.out.print("   move vehicule :)");
		}
	}
	
	public void setLiquidQuantity (int id, float quantity) {
		Vehicule v = getVehicule(id);
		if (v != null) {
			v.setLiquidQuantity(quantity);
			vRepository.save(v);
			changeVehiculeInfo(id);
		}
	}
	
	public void setFuelQuantity (int id, float quantity) {
		Vehicule v = getVehicule(id);
		if (v != null) {
			v.setFuel(quantity);
			vRepository.save(v);
			changeVehiculeInfo(id);
		}
	}
	
	public void useFuel (int id, float quantity) {
		Vehicule v = getVehicule(id);
		if (v != null) {
			float fuelNow = v.getFuel();
			v.setFuel(fuelNow - quantity);
			vRepository.save(v);
			changeVehiculeInfo(id);
		}
	}
	
	public void setCrewMember (int id, int quantity) {
		Vehicule v = getVehicule(id);
		if (v != null) {
			v.setCrewMember(quantity);
			vRepository.save(v);
			changeVehiculeInfo(id);
		}
	}
	
	public void changeEtat(int id, EtatVehicule etat) {
		Vehicule v = getVehicule(id);
		if (v != null) {
			v.setEtat(etat);
			vRepository.save(v);
		}
	}
	
	public void setidFeu (int id, int idFeu) {
		Vehicule v = getVehicule(id);
		if (v != null) {
			v.setIdFeu(idFeu);
			vRepository.save(v);
		}
	}
	
	public boolean etatAfterDeplacement(int id) { 
		boolean ret = false;
		Vehicule v = getVehicule(id);
		if (v != null) {
			if (v.getEtat() == EtatVehicule.DIRFEU) {
				v.setEtat(EtatVehicule.ATFEU);
			}
			if (v.getEtat() == EtatVehicule.DIRCASERNE) {
				v.setEtat(EtatVehicule.ATCASERNE);
			}
			vRepository.save(v);
			changeVehiculeInfo(id);
			ret = true;
		}
		return ret;
	}
	
	
	public void updateEtat() { 
		List<Vehicule> ListV = getVehiculeList();
		for (Vehicule v : ListV) {
			if (v.getEtat() == EtatVehicule.ATFEU && fService.getFireByIdSim(v.getIdFeu()) == null) {
				if (enoughFuel(v) || v.getFacilityRefID()==0) {
					v.setEtat(EtatVehicule.DISPONIBLE);
				} else {
					v.setEtat(EtatVehicule.DIRCASERNE);
				}
				v.setIdFeu(0);
			}
			if (v.getEtat() == EtatVehicule.ATCASERNE && v.getFuel() == 100) {
				v.setEtat(EtatVehicule.DISPONIBLE);
			}else if (v.getEtat() == EtatVehicule.ATCASERNE) {
				v.setFuel(v.getFuel() + 1);
				if (v.getFuel() > 100) {
					v.setFuel(100);
				}
			}
			vRepository.save(v);
			changeVehiculeInfo(v.getId());
		}
	}
	
	public boolean enoughFuel(Vehicule v) {
		boolean ret = false;
		if (v.getFacilityRefID()!= 0) {
			Caserne c = cService.getCaserne(v.getFacilityRefID());
			float fuelCons = aService.calculFuelConsumption(v.getLon(), v.getLat(), c.getLon(), c.getLat(), v.getFuelConsumption());
			if (fuelCons*2 < v.getFuel() && v.getFuel() > 25) {
				ret = true;
			} else {
				dService.genItineraire(v.getId(), c.getLon(), c.getLat(), fuelCons);
			}
		}
		return ret;
	}
	
	public void updateVehiculeRepo () {
		// On met a jour notre repo en fonction des vehicules dans le docker
		RestTemplate restTemplate = new RestTemplate();
		VehiculeDTO[] ret = restTemplate.getForObject(url_docker + "/vehicle", VehiculeDTO[].class);
		for (VehiculeDTO v : ret) {
			Optional<Vehicule> vRepo = vRepository.findByIdSim(v.getId());
			if (vRepo.isEmpty()) {
				Vehicule vSave = v.fromDTO();
				vRepository.save(vSave);
				System.out.print("CARRRRRRR");
			}
		}
	    return;
	}
	
	public Vehicule addVehicule (VehiculeDTO vDTO) {
		RestTemplate restTemplate = new RestTemplate();
		VehiculeDTO ret = restTemplate.postForObject(url_docker + "/vehicle", vDTO, VehiculeDTO.class);
		Vehicule v = vDTO.fromDTO();
		v.setIdSim(ret.getId());
		System.out.print(ret.getId());
		vRepository.save(v);
		return v;
	}
}
