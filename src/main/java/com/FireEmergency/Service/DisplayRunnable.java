package com.FireEmergency.Service;

import org.springframework.beans.factory.annotation.Autowired;

public class DisplayRunnable implements Runnable {

	private ServiceVehicule vService;
	private ServiceDeplacement dService;
	private ServiceFeu fService;
	private ServiceAffectation aService;
	private int sleepTime;
	boolean isEnd = false;
	
	public DisplayRunnable (ServiceVehicule v, ServiceDeplacement d, ServiceFeu f, ServiceAffectation a, int sleepTime) {
		this.vService = v;
		this.dService = d;
		this.aService = a;
		this.fService = f;
		this.sleepTime = sleepTime;
	}

	@Override
	public void run() {
		int i = 0;
		while (!this.isEnd) {
			try {
				System.out.println("Update1 Running....");
				Thread.sleep(sleepTime);
				fService.updateFireRepo();
				if (i == 5) { 
					aService.updateAffectation();
					i = 0;
				} else {
					i++;
				}
				//dService.updateDeplacement();
				vService.updateEtat();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("Runnable DisplayRunnable ends.... ");
	}

	public void stop() {
		this.isEnd = true;
	}

}
