package com.FireEmergency.Service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.FireEmergency.Model.Deplacement;
import com.FireEmergency.Model.Vehicule;
import com.FireEmergency.Model.Waypoint;
import com.FireEmergency.Model.mapBoxDTO;
import com.FireEmergency.Repository.RepoDeplacement;
import com.FireEmergency.Service.ServiceVehicule;

@Service
public class ServiceDeplacement {
	
	@Autowired
	RepoDeplacement dRepository;
	
	@Autowired
	ServiceVehicule vService;
	
	//Distance parcouru a chaque appel d'updateDeplacement
	double distanceBase = 0.0010;
	//double distanceBase = 0.020;
	
	String url_mapbox = "https://api.mapbox.com/directions/v5/mapbox/driving/";
	String mapboxSettings = "?geometries=geojson&access_token=";
	String mapboxKey = "pk.eyJ1IjoidGhlbWRlaWwiLCJhIjoiY2twZTBuMXBzMXNzNjJubnhkdjU2YWVnaSJ9.JQOr5slR2vCCmEUEWNjI2A";
			
	public ServiceDeplacement () {
		
	}
	
	public mapBoxDTO getMapBoxDTO() {
		RestTemplate restTemplate = new RestTemplate();
		mapBoxDTO m = restTemplate.getForObject(url_mapbox, mapBoxDTO.class);
		return m;
	}
	
	public Deplacement getDeplacement (int id_v) {
		Optional<Deplacement> d = dRepository.findByIdV(id_v);
		if (d.isPresent()) {
			return d.get();
		}else {
			return null;
		}
	}
	
	public List<Point2D.Double> getItineraire (int id_v) {
		Optional<Deplacement> d = dRepository.findByIdV(id_v);
		if (d.isPresent()) {
			Deplacement dep = d.get();
			return dep.getWaypoints().subList(dep.getWPiterator(), dep.getWaypoints().size());
		}else {
			return null;
		}
	}
	
	public List<Deplacement> getListDeplacement() {
		Iterable<Deplacement> listeDeplacementIterable = dRepository.findAll();
		List<Deplacement> listeDeplacement = new ArrayList<Deplacement>();
		listeDeplacementIterable.forEach(listeDeplacement::add);
		return listeDeplacement;
	}
	
	public void genItineraire(int id_v, double lonFeu, double latFeu, float fuel) {
		//Recuperatio du vehicule
		Vehicule v = vService.getVehicule(id_v);
		//Entite Mapbox
		String url_request = url_mapbox + String.valueOf(v.getLon()) + "," + String.valueOf(v.getLat())
							 + ";" + String.valueOf(lonFeu) + "," + String.valueOf(latFeu)
							 + mapboxSettings + mapboxKey;
		RestTemplate restTemplate = new RestTemplate();
		mapBoxDTO m = restTemplate.getForObject(url_request, mapBoxDTO.class);
		//List of waypoints de mapbox
		List<List<Double>> MBwaypoints = m.getRoutes().get(0).getGeometry().getCoordinates();
		//List of waypoints pour itineraire
		List<Point2D.Double> waypoints = new ArrayList<Point2D.Double>();
		//Position vehicule
		Point2D.Double start = new Point2D.Double(v.getLon(), v.getLat());
		waypoints.add(start);
		//Positions intermediaires
		for (List<Double> WP : MBwaypoints) {
			Point2D.Double wp = new Point2D.Double(WP.get(0), WP.get(1));
			waypoints.add(wp);
		}
		//Position feu/caserne
		Point2D.Double end = new Point2D.Double(lonFeu, latFeu);
		waypoints.add(end);
		Deplacement d = new Deplacement(id_v, waypoints, fuel);
		updateVector(d);
		dRepository.save(d);
	}
	
	public void updateDeplacement() {
		List<Deplacement> deplacements = getListDeplacement();
		System.out.println(deplacements.size());
		for (Deplacement d :deplacements) {
			if (d.getEtat() == "Moving") {
				if (d.getWaypoints().get(d.getWPiterator()+1).distance(d.getLon(), d.getLat()) > distanceBase) {
					//System.out.println("ca bouge depuis: " + d.getLon());
					d.setLon(d.getLon() + d.getVectLon());
					d.setLat(d.getLat() + d.getVectLat());
					vService.moveVehicule(d.getIdV(), d.getLon(), d.getLat());
					//System.out.println("ca bouge en: " + d.getLon());
				} else {
					d.setWPiterator(d.getWPiterator() + 1);
					d.setLon(d.getWaypoints().get(d.getWPiterator()).getX());
					d.setLat(d.getWaypoints().get(d.getWPiterator()).getY());
					vService.moveVehicule(d.getIdV(), d.getLon(), d.getLat());
					//System.out.println("ca bouge mais pas trop");
					if (d.getWPiterator() == d.getWaypoints().size()-1) {
						// fin du deplacement (appel fct d'attente exinction/plein en caserne)
						d.setEtat("Arrived");
					} else {
						updateVector(d);
					}
				}
				dRepository.save(d);
			} if (d.getEtat() == "Arrived") {
				// attente extinction feu/ refuel...
				boolean ret = vService.etatAfterDeplacement(d.getIdV());
				if (ret) {
					vService.useFuel(d.getIdV(), d.getFuel());
					dRepository.delete(d);
				}
			}
		}
	}
	
	public void updateVector(Deplacement d) {
		int i = d.getWPiterator();
		List<Point2D.Double> waypoints = d.getWaypoints();
		double norme = Math.sqrt(Math.pow((waypoints.get(i+1).getX()-waypoints.get(i).getX()),2) + 
	             Math.pow((waypoints.get(i+1).getY()-waypoints.get(i).getY()),2));
		d.setVectLon(((waypoints.get(i+1).getX()-waypoints.get(i).getX())/norme)*distanceBase);
		d.setVectLat(((waypoints.get(i+1).getY()-waypoints.get(i).getY())/norme)*distanceBase);
	}
}
