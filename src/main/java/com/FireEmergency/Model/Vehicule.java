package com.FireEmergency.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class Vehicule {

	//private int CREW_MEMBER_START_VALUE=-1;
	@Id
	@GeneratedValue (generator="cars")
	private Integer id;
	private Integer idSim;
	private double lon;
	private double lat;
	private VehiculeType type;
	private float efficiency; 
	private LiquidType liquidType; 
	private float liquidQuantity; 
	private float liquidConsumption; 
	private float fuel;		
	private float fuelConsumption; 
	private int crewMember;
	private int crewMemberCapacity;
	private Integer facilityRefID;
	//new attributes
	private int idFeu;
	private EtatVehicule etat;
	
	public Vehicule () {
		
	}

	public Vehicule (Integer id_sim,
			double lon,
			double lat,
			VehiculeType type,
			float efficiency,
			LiquidType liquidType,
			float liquidQuantity, 
			float liquidConsumption,
			float fuel,	
			float fuelConsumption,
			int crewMember,
			int crewMemberCapacity,
			Integer facilityRefID)	{
		
		this.idSim = id_sim;
		this.lon = lon;
		this.lat = lat;
		this.type = type;
		this.efficiency = efficiency;
		this.liquidType = liquidType;
		this.liquidQuantity = liquidQuantity;
		this.liquidConsumption = liquidConsumption;
		this.fuel = fuel;
		this.fuelConsumption = fuelConsumption;
		this.crewMember = crewMember;
		this.crewMemberCapacity = crewMemberCapacity;
		this.facilityRefID = facilityRefID;
		//Nouveaux attribus en dessous
		this.idFeu = 0;
		this.etat = EtatVehicule.DISPONIBLE;
	}
	
	public EtatVehicule getEtat() {
		return etat;
	}
	
	public void setEtat(EtatVehicule etat) {
		this.etat = etat;
		return;
	}
	
	public int getIdFeu() {
		return idFeu;
	}

	public void setIdFeu(int idFeu) {
		this.idFeu = idFeu;
	}

	public VehiculeDTO toDTO () {
		VehiculeDTO v = new VehiculeDTO(idSim, lon, lat, type, efficiency, liquidType,
									  liquidQuantity, liquidConsumption, fuel, fuelConsumption, 
									  crewMember, crewMemberCapacity, facilityRefID);
		return v;
	}
	
	public Integer getIdSim() {
		return idSim;
	}
	
	public void setIdSim(Integer id) {
		this.idSim = id;
	}
	
	public Integer getId() {
		return id;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public VehiculeType getType() {
		return type;
	}

	public void setType(VehiculeType type) {
		this.type = type;
	}

	public float getEfficiency() {
		return efficiency;
	}

	public void setEfficiency(float efficiency) {
		this.efficiency = efficiency;
	}

	public LiquidType getLiquidType() {
		return liquidType;
	}

	public void setLiquidType(LiquidType liquidType) {
		this.liquidType = liquidType;
	}

	public float getLiquidQuantity() {
		return liquidQuantity;
	}

	public void setLiquidQuantity(float liquidQuantity) {
		this.liquidQuantity = liquidQuantity;
	}

	public float getLiquidConsumption() {
		return liquidConsumption;
	}

	public void setLiquidConsumption(float liquidConsumption) {
		this.liquidConsumption = liquidConsumption;
	}

	public float getFuel() {
		return fuel;
	}

	public void setFuel(float fuel) {
		this.fuel = fuel;
	}

	public float getFuelConsumption() {
		return fuelConsumption;
	}

	public void setFuelConsumption(float fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}

	public int getCrewMember() {
		return crewMember;
	}

	public void setCrewMember(int crewMember) {
		this.crewMember = crewMember;
	}

	public int getCrewMemberCapacity() {
		return crewMemberCapacity;
	}

	public void setCrewMemberCapacity(int crewMemberCapacity) {
		this.crewMemberCapacity = crewMemberCapacity;
	}

	public Integer getFacilityRefID() {
		return facilityRefID;
	}

	public void setFacilityRefID(Integer facilityRefID) {
		this.facilityRefID = facilityRefID;
	}

	
}
