package com.FireEmergency.Model;

public enum EtatVehicule {

	DISPONIBLE,
	DIRFEU,
	DIRCASERNE,
	ATFEU,
	ATCASERNE;
	
}
