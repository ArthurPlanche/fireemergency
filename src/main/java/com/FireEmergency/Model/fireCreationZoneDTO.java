package com.FireEmergency.Model;

import java.util.ArrayList;
import java.util.List;

public class fireCreationZoneDTO {
	String type;
	
	List<Integer> coordinates;
	public fireCreationZoneDTO() {};
	public fireCreationZoneDTO(int Coord1,int Coord2) {
		this.type = "Point";
		List<Integer> coord = new ArrayList<Integer>();
		coord.add(Coord1);
		coord.add(Coord2);
		this.coordinates = coord;
	}
	
	public String getType() {
		return type;
	}

	public List<Integer> getCoordinates() {
		return coordinates;
	}

}
