package com.FireEmergency.Model;

public class Route {

	private float duration;
	private float distance;
	private Geometry geometry;
	
	public float getDuration() {
		return duration;
	}
	public float getDistance() {
		return distance;
	}
	public Geometry getGeometry() {
		return geometry;
	}
	
}
