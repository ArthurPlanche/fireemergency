package com.FireEmergency.Model;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.awt.geom.Point2D;
import java.util.List;

@Entity
public class Deplacement {
	
	@Id
	@GeneratedValue (generator="moveIT")
	private Integer id;
	private Integer idV;
	@ElementCollection(fetch = FetchType.EAGER)
	private List<Point2D.Double> waypoints;
	private Integer WPiterator;
	private double lon;
	private double lat;
	private double vectLon;
	private double vectLat;
	private float UsedFuel;
	private String Etat;
	
	public Deplacement () {
		
	}
	
	public Deplacement (int id_v, List<Point2D.Double> waypoints, float fuel) {
		this.idV = id_v;
		lon = waypoints.get(0).getX();
		lat = waypoints.get(0).getY();
		this.waypoints = waypoints;
		WPiterator = 0;
		vectLon = 0;
		vectLat = 0;
		Etat = "Moving";
		UsedFuel = fuel;
	}
	
	public float getFuel() {
		return UsedFuel;
	}
	
	public String getEtat() {
		return Etat;
	}
	
	public void setEtat(String etat) {
		Etat = etat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public List<Point2D.Double> getWaypoints() {
		return waypoints;
	}

	public void setWaypoints(List<Point2D.Double> waypoints) {
		this.waypoints = waypoints;
	}

	public Integer getWPiterator() {
		return WPiterator;
	}

	public void setWPiterator(Integer wPiterator) {
		WPiterator = wPiterator;
	}

	public double getVectLon() {
		return vectLon;
	}

	public void setVectLon(double vectLon) {
		this.vectLon = vectLon;
	}

	public double getVectLat() {
		return vectLat;
	}

	public void setVectLat(double vectLat) {
		this.vectLat = vectLat;
	}

	public Integer getId() {
		return id;
	}

	public Integer getIdV() {
		return idV;
	}
	
	
	
}
