package com.FireEmergency.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Caserne {
	
	@Id
	@GeneratedValue (generator="thomasScalise")
	private Integer id;
	private double lon;
	private double lat;
	private Integer capacity;
	
	public Caserne() {}
	
	public Caserne(double lon, double lat, int capacity) {
		this.lon = lon;
		this.lat = lat;
		this.capacity = capacity;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public Integer getId() {
		return id;
	}
	
	
	

}
