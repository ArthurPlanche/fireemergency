package com.FireEmergency.Model;


public class FireDTO {
		
		private Integer id;
		private FireType type;
		private float intensity;
		private float range;
		private double lon;
		private double lat;
		
		public FireDTO() {};
		
		public FireDTO(Integer id, FireType type, float intensity, float range, double lon, double lat) {
			this.id = id;
			this.type = type;
			this.intensity = intensity;
			this.range = range;
			this.lon = lon;
			this.lat = lat;
		}
		
		public Integer getId() {
			return id;
		}
		
		
		public FireType getType() {
			return type;
		}
		
		public float getIntensity() {
			return intensity;
		}
		
		public float getRange() {
			return range;
		}
		
		public double getLon() {
			return lon;
		}
		
		public double getLat() {
			return lat;
		}
		
		public Fire fromDTO () {
			Fire v = new Fire(id, type, intensity, range, lon, lat);
			return v;
		}

		

	}


