package com.FireEmergency.Model;

import java.util.ArrayList;
import java.util.List;

public class ConfigDTO {
	double fireCreationProbability ;
    int fireCreationSleep;
    

	List<fireCreationZoneDTO> fireCreationZone; 
    double max_INTENSITY;
    double max_RANGE;

    

	public ConfigDTO() {
    	
    }
    
    public ConfigDTO(double proba) {
    	this.fireCreationProbability = proba;
    	this.fireCreationSleep=20000;
    	
		
		List<fireCreationZoneDTO> coord = new ArrayList<fireCreationZoneDTO>();
		coord.add(new fireCreationZoneDTO(520820,5719535)); coord.add(new
		fireCreationZoneDTO(566984,5754240)); this.fireCreationZone = coord;
		 
    	
    	this.max_INTENSITY = 50.0;
    	this.max_RANGE = 50.0;
    	
    }
    public int getFireCreationSleep() {
		return fireCreationSleep;
	}

	public List<fireCreationZoneDTO> getFireCreationZone() {
		return fireCreationZone;
	}

	public double getMax_INTENSITY() {
		return max_INTENSITY;
	}

	public double getMax_RANGE() {
		return max_RANGE;
	}
    public double getFireCreationProbability() {
		return fireCreationProbability;
	}

	public void setFireCreationProbability(double fireCreationProbability) {
		this.fireCreationProbability = fireCreationProbability;
	}
	
    @Override
    public String toString() {
		return "Config: proba:"+getFireCreationProbability();
	}

}
