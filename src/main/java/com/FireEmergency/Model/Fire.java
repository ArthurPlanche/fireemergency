package com.FireEmergency.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Fire {
	@Id
	@GeneratedValue (generator="fires")
	private Integer id;
	private Integer idSim;
	private FireType type;
	private float intensity;
	private float range;
	private double lon;
	private double lat;
	private boolean affectation = false;
	
	public Fire() {	
	}
	
	public Fire(Integer idSim,FireType type,float intensity,
			 float range,double lon,double lat) {
		this.idSim=idSim;
		this.type=type;
		this.intensity=intensity;
		this.range=range;
		this.lon=lon;
		this.lat=lat;
		this.affectation = false;
		
	}

	
	public boolean getAffectation() {
		return affectation;
	}

	public void setAffectation(boolean affectation) {
		this.affectation = affectation;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getIdSim() {
		return idSim;
	}
	public void setIdSim(Integer idSim) {
		this.idSim = idSim;
	}
	public FireType getType() {
		return type;
	}
	public void setType(FireType type) {
		this.type = type;
	}
	public float getIntensity() {
		return intensity;
	}
	public void setIntensity(float intensity) {
		this.intensity = intensity;
	}
	public float getRange() {
		return range;
	}
	public void setRange(float range) {
		this.range = range;
	}
	public double getLon() {
		return lon;
	}
	public void setLon(double lon) {
		this.lon = lon;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	
	@Override
	public String toString() {
		return "Fire: id:"+getId()+" idSim:"+getIdSim()+" Coord [lon:"+getLon()+" lat:"+getLat()+"] range:"+getRange()+" Type:"+getType()+" intensity:"+getIntensity();
	}
	
}
