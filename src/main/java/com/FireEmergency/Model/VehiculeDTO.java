package com.FireEmergency.Model;

public class VehiculeDTO {
	
	//private int CREW_MEMBER_START_VALUE=-1;
	private Integer id;
	private double lon;
	private double lat;
	private VehiculeType type;
	private float efficiency; 
	private LiquidType liquidType; 
	private float liquidQuantity; 
	private float liquidConsumption; 
	private float fuel;		
	private float fuelConsumption; 
	private int crewMember;
	private int crewMemberCapacity;
	private Integer facilityRefID;
	
	public VehiculeDTO () {
		
	}
	
	public VehiculeDTO (Integer id,
						double lon,
						double lat,
						VehiculeType type,
						float efficiency,
						LiquidType liquidType,
						float liquidQuantity, 
						float liquidConsumption,
						float fuel,	
						float fuelConsumption,
						int crewMember,
						int crewMemberCapacity,
						Integer facilityRefID)	{
		this.id = id;
		this.lon = lon;
		this.lat = lat;
		this.type = type;
		this.efficiency = efficiency;
		this.liquidType = liquidType;
		this.liquidQuantity = liquidQuantity;
		this.liquidConsumption = liquidConsumption;
		this.fuel = fuel;
		this.fuelConsumption = fuelConsumption;
		this.crewMember = crewMember;
		this.crewMemberCapacity = crewMemberCapacity;
		this.facilityRefID = facilityRefID;
	}
	
	public void setId(int id) {
		this.id = id;
		return;
	}
	
	public int getId () {
		return this.id;
	}
	
	public double getLon() {
		return lon;
	}
	
	public double getLat() {
		return lat;
	}

	public VehiculeType getType() {
		return type;
	}

	public float getEfficiency() {
		return efficiency;
	}

	public LiquidType getLiquidType() {
		return liquidType;
	}

	public float getLiquidQuantity() {
		return liquidQuantity;
	}

	public float getLiquidConsumption() {
		return liquidConsumption;
	}

	public float getFuel() {
		return fuel;
	}

	public float getFuelConsumption() {
		return fuelConsumption;
	}

	public int getCrewMember() {
		return crewMember;
	}

	public int getCrewMemberCapacity() {
		return crewMemberCapacity;
	}

	public Integer getFacilityRefID() {
		return facilityRefID;
	}
	
	public Vehicule fromDTO () {
		Vehicule v = new Vehicule(id, lon, lat, type, efficiency, liquidType,
				  liquidQuantity, liquidConsumption, fuel, fuelConsumption, 
				  crewMember, crewMemberCapacity, facilityRefID);
		return v;
	}

}
