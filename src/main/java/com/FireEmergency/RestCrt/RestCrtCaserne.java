package com.FireEmergency.RestCrt;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.FireEmergency.Model.Caserne;
import com.FireEmergency.Model.Vehicule;
import com.FireEmergency.Model.VehiculeDTO;
import com.FireEmergency.Service.ServiceCaserne;

@CrossOrigin
@RestController
@RequestMapping("/Caserne")
public class RestCrtCaserne {
	
	@Autowired
	ServiceCaserne cService;
	
	@PostMapping(value="/newCaserne")
	public Caserne newCaserne(@RequestParam double lon, @RequestParam double lat, @RequestParam int cap) {
		Caserne cRet = cService.newCaserne(lon, lat, cap);
		return cRet;
	}
	
	@GetMapping(value="/casernes")
	public List<Caserne> getCaserneList() {
		List<Caserne> ListCaserne = cService.getListCaserne();
		return ListCaserne;
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/modif/{id}")
	public void changeCaserne(@PathVariable int id, @RequestBody Caserne c) {
		cService.changeCaserne(id, c);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/delete/{id}")
	public void delCaserne(@PathVariable int id) {
		cService.delCaserne(id);
		return;
	}

}
