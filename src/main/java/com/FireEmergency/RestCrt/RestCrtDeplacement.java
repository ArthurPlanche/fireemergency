package com.FireEmergency.RestCrt;

import java.awt.geom.Point2D;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.FireEmergency.Model.Deplacement;
import com.FireEmergency.Model.mapBoxDTO;
import com.FireEmergency.Service.ServiceDeplacement;

@CrossOrigin
@RestController
@RequestMapping("/Deplacement")
public class RestCrtDeplacement {
	
	@Autowired
	ServiceDeplacement dService;
	
	@GetMapping(value="/getDep/{id_v}")
	public Deplacement getDep (@PathVariable int id_v) {
		Deplacement d = dService.getDeplacement(id_v);
		return d;
	}
	
	@GetMapping(value="/getIti/{id_v}")
	public List<Point2D.Double> getIti (@PathVariable int id_v) {
		List<Point2D.Double> iti = dService.getItineraire(id_v);
		return iti;
	}
	
	@GetMapping(value="/genIti")
	public void deplacerV (@RequestParam int id_v, @RequestParam double lonFeu, @RequestParam double latFeu) {
		dService.genItineraire(id_v, lonFeu, latFeu, 0);
	}
	
	@GetMapping(value="/update")
	public void update () { 
		dService.updateDeplacement();
	}
	
	@GetMapping(value="/mapbox")
	public mapBoxDTO mapbox () {
		mapBoxDTO m = dService.getMapBoxDTO();
		return m;
	}

}
