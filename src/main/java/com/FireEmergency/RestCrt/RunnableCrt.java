package com.FireEmergency.RestCrt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.FireEmergency.Service.DisplayRunnable;
import com.FireEmergency.Service.UpdateService;

@CrossOrigin
@RestController
@RequestMapping("/run")
public class RunnableCrt {
	
	@Autowired
	UpdateService uService;
	
	@RequestMapping(method=RequestMethod.GET,value="/stop")
	public void stopDisplay() {
		uService.stopDisplay();
	}


}
