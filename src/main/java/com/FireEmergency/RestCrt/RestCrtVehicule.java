package com.FireEmergency.RestCrt;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.FireEmergency.Service.ServiceVehicule;
import com.FireEmergency.Model.Vehicule;
import com.FireEmergency.Model.VehiculeDTO;

@CrossOrigin
@RestController
@RequestMapping("/Vehicule")
public class RestCrtVehicule {
	
	@Autowired
	ServiceVehicule vService;

	@GetMapping(value="/vehicules")
	public List<Vehicule> getVehiculeList() {
		List<Vehicule> ListVehicule = vService.getVehiculeList();
		return ListVehicule;
	}
	
	@GetMapping(value="/vehicule/{id}")
	public Vehicule getVehicule(@PathVariable int id) {
		Vehicule Vehicule = vService.getVehicule(id);
		return Vehicule;
	}
	
	@PostMapping(value="/newVehicule")
	public Vehicule newVehicule(@RequestBody VehiculeDTO v) {
		Vehicule vRet = vService.addVehicule(v);
		System.out.print("NEW VEHICULE");
		return vRet;
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/modif/{id}")
	public void changeVehicule(@PathVariable int id, @RequestBody VehiculeDTO vDTO) {
		vService.changeVehiculeFromDTO(id, vDTO);
	}

	@RequestMapping(method=RequestMethod.DELETE, value="/delete/{id}")
	public void delVehicule(@PathVariable int id) {
		vService.delVehicule(id);
		return;
	}
	

}
