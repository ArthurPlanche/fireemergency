package com.FireEmergency.RestCrt;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.FireEmergency.Model.ConfigDTO;
import com.FireEmergency.Model.Fire;
import com.FireEmergency.Service.ServiceFeu;

@CrossOrigin
@RestController
@RequestMapping("/Fire")
public class RestCrtFeu {
	@Autowired
    ServiceFeu fService;
	
	@GetMapping(value="/fires")
	public List<Fire> getFires() {
		List<Fire> ListFire = fService.getFireList();
		return ListFire; //.header("Access-Control-Allow-Origin", "*");
	}
	
	@PostMapping(value="/config")
	public void configFire(@RequestBody ConfigDTO config) {
		fService.changeFeuConfig(config);	
	}
	
	@GetMapping(value="/getConfig")
	public ConfigDTO GetConfigFire() {
		ConfigDTO ConfigFeu = fService.getConfig();
		
		return ConfigFeu;
	}
	
	@GetMapping(value="/reset")
	public void resetFire() {
		fService.resetFeu();	
	}
}
