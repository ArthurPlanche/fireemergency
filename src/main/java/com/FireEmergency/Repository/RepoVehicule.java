package com.FireEmergency.Repository;


import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

import com.FireEmergency.Model.Vehicule;


public interface RepoVehicule extends CrudRepository<Vehicule, Integer> {

    public Optional<Vehicule> findByIdSim(int id);

}

