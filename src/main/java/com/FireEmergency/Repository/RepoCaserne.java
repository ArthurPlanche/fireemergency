package com.FireEmergency.Repository;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

import com.FireEmergency.Model.Caserne;


public interface RepoCaserne extends CrudRepository<Caserne, Integer> {

    public Optional<Caserne> findById(int id);

}
