package com.FireEmergency.Repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

import com.FireEmergency.Model.Deplacement;

public interface RepoDeplacement extends CrudRepository<Deplacement, Integer>{
	public Optional<Deplacement> findByIdV(int IdV);
}