package com.FireEmergency.Repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.FireEmergency.Model.Fire;

public interface RepoFire extends CrudRepository<Fire, Integer>{
	public Optional<Fire> findByIdSim(int IdSim);
}
