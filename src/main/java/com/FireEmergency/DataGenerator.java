package com.FireEmergency;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.FireEmergency.Service.ServiceCaserne;
import com.FireEmergency.Service.ServiceVehicule;

@Component
public class DataGenerator implements ApplicationRunner {
	
	@Autowired
	ServiceCaserne cService;
	
	@Autowired
	ServiceVehicule vService;
		
	@Override
	public void run(ApplicationArguments args) throws Exception {
		cService.newCaserne(4.833673990876717, 45.758257472061594, 10);
		cService.newCaserne(4.909, 45.7338, 10);
	}


}

