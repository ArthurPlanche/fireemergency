function displayInfoCaserneMap() {
    const context = {
        method: 'GET',
        headers: {
            'content-type': 'application/json'
        }
    }
    fetch('http://localhost:8082/Caserne/casernes', context)  //sends a request of GET type to our services 
        .then(response => response.json().then(body => displayCaserne(body)))
        .catch(error => console.log(error))
    setTimeout(displayInfoCaserneMap, 10000);
}

flagCaserne = false;
function displayCaserne(body){ // function goal is to update our page with the cursors of the "casernes"
    if (!flagCaserne) {
        mapIdCaserneLayerOld = new Map();
        flagCaserne = true;
    }

    mapIdCaserneLayerNew = new Map();
    for(cas in body){
        caserne = body[cas]
        if (!mapIdCaserneLayerOld.has(caserne.id)) {

            var Caserne = L.marker([caserne.lat, caserne.lon], {
                icon: CaserneIcon
            }).addTo(mymap);
            Caserne.bindPopup("Id : "+caserne.id+"<br>Position :"+caserne.lat+","+caserne.lon+"<br>Capacity :"+caserne.capacity)
            mapIdCaserneLayerNew.set(caserne.id,[caserne.lat,caserne.lon,caserne.cap,Caserne]);
        }
        else{
            mapIdCaserneLayerOld.get(caserne.id)[3].getPopup().setContent("Id : "+caserne.id+"<br>Position :"+caserne.lat+","+caserne.lon+"<br>Capacity :"+caserne.capacity + "<br><audio autoplay src='../media/office-sounds.mp3'>");
            mapIdCaserneLayerNew.set(caserne.id,[mapIdCaserneLayerOld.get(caserne.id)[0],mapIdCaserneLayerOld.get(caserne.id)[1],mapIdCaserneLayerOld.get(caserne.id)[2],mapIdCaserneLayerOld.get(caserne.id)[3]]);
            mapIdCaserneLayerOld.delete(caserne.id);
        }
    }
    if (mapIdCaserneLayerOld.size != 0){
        mapIdCaserneLayerOld.forEach(function(value, key) {
            mymap.removeLayer(value[3]);//we remove the marker from the map
            mapIdCaserneLayerOld.delete(key);//we delete the caserne from the old map
        });
    }
    mapIdCaserneLayerOld = new Map(mapIdCaserneLayerNew);  
}


var AddCasernePopup = L.popup().setContent('Where do you need a firehouse ? <br><form onsubmit=AddCaserne(event) method="POST" id="Add"><label for="latEdit">Latitude (between 45.666 and 45.8373):</label><br><input type="number" id="latEdit" name="latEdit" min="45.666" max="45.8373" step="0.0001"><br><label for="lonEdit">Longitude (between 4.688 and 4.97):</label><br><input type="number" id="lonEdit" name="lonEdit" min="4.688" max="4.97"  step="0.001"><input type="submit"></form>');
AddCaserneButton = L.easyButton('<img title = "Add a firehouse" src="../media/tinycaserne.png">', function (btn, map) {
    AddCasernePopup.setLatLng(map.getCenter()).openOn(map);
}).addTo(mymap);

function AddCaserne(event){ //function goal is to add a new caserne on the page if needed
    event.preventDefault();
    mymap.closePopup();

    const context = {
        method: 'POST',
        headers: {
            'Content-type': 'application/x-www-form-urlencoded'
        },
        body: "lon="+document.getElementById('lonEdit').value+"&lat="+document.getElementById('latEdit').value+"&cap=8"
    }
    fetch("http://localhost:8082/Caserne/newCaserne", context)
        .then()
        .catch(error => console.log(error))

}

displayInfoCaserneMap();
