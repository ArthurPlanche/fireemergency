function displayInfoMap(){
    const context = {
        method: 'GET',
        headers: {
            'content-type': 'application/json'
        } 
    }
    fetch('http://localhost:8082/Fire/fires', context)//get the list of all the fire
    .then(response => response.json().then(body => displayFire(body)))
    .catch(error => console.log(error))
    setTimeout(displayInfoMap, 2000);//refresh every 5sec
}

function getColor(i) {//get a color in function of the intensity of the fire
    return i > 49 ? '#800026' :
            i > 35  ? '#E31A1C' :
            i > 15   ? '#FD8D3C' :
            i > 0   ? '#FED976' :
                        '#FFEDA0';
}

function displayFire(body){

    mapIdFireLayerNew = new Map(); 
        
    for(const fire of body){

        if (!mapIdFireLayerOld.has(fire.id)){//if the fire doesn't have a marker
            var circle = L.circle([fire.lat, fire.lon], {//creation of a marker
                color: getColor(fire.intensity),
                fillColor: getColor(fire.intensity),
                fillOpacity: 1.0,
                radius: fire.range
            })

            switch (fire.type){//add the marker to a layer group to be able to filter the fire with his type
                case 'A':
                    type = A;
                    circle.addTo(A);
                    break;
                case 'B_Gasoline':
                    type = B_Gasoline;
                    circle.addTo(B_Gasoline);
                    break;
                case 'B_Alcohol':
                    type = B_Alcohol;
                    circle.addTo(B_Alcohol);
                    break;
                case 'B_Plastics':
                    type = B_Plastics;
                    circle.addTo(B_Plastics);
                    break;
                case 'C_Flammable_Gases':
                    type = C_Flammable_Gases;
                    circle.addTo(C_Flammable_Gases);
                    break;
                case 'D_Metals':
                    type = D_Metals;
                    circle.addTo(D_Metals);
                    break;
                case 'E_Electric':
                    type = E_Electric;
                    circle.addTo(E_Electric);
                    break;
                default:
                    break;
            }

            circle.bindPopup("<img alt='fire image' src=../media/fire.png height=25 lenght=25><br>Fire " + fire.type + "<br>Intensity: " + fire.intensity + "<br>Range: " + fire.range);
            //add popup to the marker to display the information about the fire

            if (fire.intensity > 49){//add the fire to a layer group based on his intensity
                group = feu_fou;
                circle.addTo(feu_fou);
            }
            else if (fire.intensity > 35){
                group = feu_gros;
                circle.addTo(feu_gros);
            }
            else if (fire.intensity > 15){
                group = feu_moyen;
                circle.addTo(feu_moyen);
            }
            else{
                group = depart_feu;
                circle.addTo(depart_feu);
            }
            mapIdFireLayerNew.set(fire.id,[circle,group,type])//add the fire to a map (=dico in python), key = id fire, value = [marker,layergroupIntensity,layergroupType]
        }
        else {//if the fire has a marker
            mapIdFireLayerOld.get(fire.id)[0].getPopup().setContent("<img src=../media/fire.png height=25 lenght=25><br>Fire " + fire.type + "<br>Intensity: " + fire.intensity + "<br>Range: " + fire.range + "<br><audio autoplay src='https://lasonotheque.org/UPLOAD/mp3/1340.mp3'>");
            //we update his information in the popup

            if (fire.intensity > 49){
                group = feu_fou;
            }
            else if (fire.intensity > 35){
                group = feu_gros;
            }
            else if (fire.intensity > 15){
                group = feu_moyen;
            }
            else{
                group = depart_feu;
            }

            if (group._leaflet_id != mapIdFireLayerOld.get(fire.id)[1]._leaflet_id){//if he is in a different intensity category
                mapIdFireLayerOld.get(fire.id)[1].removeLayer(mapIdFireLayerOld.get(fire.id)[0]);//we remove the marker from his old layergroup
                mapIdFireLayerOld.get(fire.id)[0].addTo(group);//we add the marker to his new layergroup
                mapIdFireLayerOld.get(fire.id)[0].setStyle({//we change the color of the marker
                    color: getColor(fire.intensity),
                    fillColor: getColor(fire.intensity)
                })
            }
            mapIdFireLayerNew.set(fire.id,[mapIdFireLayerOld.get(fire.id)[0],group,mapIdFireLayerOld.get(fire.id)[2]]);//add the fire to the new map
            mapIdFireLayerOld.delete(fire.id);//delete the fire from the old map
        }
    }

    if (mapIdFireLayerOld.size != 0){//if there is still fires in the old map, it means that there is fire that doesn't exist anymore
        mapIdFireLayerOld.forEach(function(value, key) {//so for every fire that died
            value[1].removeLayer(value[0]);//we remove the marker from the layer groups
            value[2].removeLayer(value[0]);
            mymap.removeLayer(value[0]);//we remove the marker from the map
            mapIdFireLayerOld.delete(key);//we delete the fire from the old map
          });
    }

    mapIdFireLayerOld = new Map(mapIdFireLayerNew);//we copy the new map into the old one
}

L.easyButton('<img title="Reset Fire" src="../media/fire.png" height=18 lenght=18>', function(){//button for the reset of all the fire 
    const context = {
        method: 'GET'
    }
    fetch('http://localhost:8082/Fire/reset', context)//reset all the fire
    .then()
    .catch(error => console.log(error))
}).addTo(mymap);

//button to change the config of the fires with a form
//fetch the current config and call createpopup function
L.easyButton('<img title="Config Fire" src="../media/configFire.png" height=20 lenght=20>', function(btn, map){
    const context = {
        method: 'GET'
    }

    fetch('http://localhost:8082/Fire/getConfig', context)//get config of the fires
    .then(response => response.json().then(body => createPopup(body)))
    .catch(error => console.log(error))
     
}).addTo(mymap);

function createPopup(body){//function to create the popup with the current values of the config as default values
    fireCreationProbability='<label for="fireCreationProbability">Enter the fireCreationProbability:</label> <input type="number" step=".01" name="fireCreationProbability" id="fireCreationProbability" value='+body.fireCreationProbability+' required><br>';
    fireCreationSleep='<label for="fireCreationSleep">Enter the fireCreationSleep:</label> <input type="number" step=".01" name="fireCreationSleep" id="fireCreationSleep" value='+body.fireCreationSleep+' required><br>';
    max_INTENSITY='<label for="max_INTENSITY">Enter the max_INTENSITY:</label> <input type="number" step=".01" name="max_INTENSITY" id="max_INTENSITY" value='+body.max_INTENSITY+' required><br>';
    max_RANGE='<label for="max_RANGE">Enter the max_RANGE:</label> <input type="number" step=".01" name="max_RANGE" id="max_RANGE" value='+body.max_RANGE+' required><br>';
    addPopup = L.popup().setContent('<b>Configure the fire</b><br><form onsubmit=ChangeFire(event) method="POST" id="ChangeFire">'+fireCreationProbability+fireCreationSleep+max_INTENSITY+max_RANGE+'<input type="submit"></input</form>');
    addPopup.setLatLng(mymap.getCenter()).openOn(mymap);
}

function ChangeFire(event){//function to send the http request to change the fire config
    event.preventDefault();//avoid using the function without using the form
    mymap.closePopup();
    requete = {//create the body of the request with the values of the form
        "fireCreationProbability": document.getElementById("fireCreationProbability").value,
        "fireCreationSleep": document.getElementById("fireCreationSleep").value,
        "fireCreationZone": [
            {
                "type": "Point",
                "coordinates": [
                    520820,
                    5719535
                ]
            },
            {
                "type": "Point",
                "coordinates": [
                    566984,
                    5754240
                ]
            }
        ],
        "max_INTENSITY": document.getElementById("max_INTENSITY").value,
        "max_RANGE": document.getElementById("max_RANGE").value
    }

    const context = {//creating the request
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
            },
        body: JSON.stringify( requete ) 
    }

    fetch('http://localhost:8082/Fire/config', context)//change config of the fires
    .then()
    .catch(error => console.log(error))
}

mapIdFireLayerOld = new Map();
displayInfoMap();//start the loop to refresh the fires on the map