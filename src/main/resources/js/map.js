var mymap = L.map('mapid').setView([45.756, 4.83], 12);

//creation filtre sur type et intensite

var A = L.layerGroup();
var B_Gasoline = L.layerGroup();
var B_Alcohol = L.layerGroup();
var B_Plastics = L.layerGroup();
var C_Flammable_Gases = L.layerGroup();
var D_Metals = L.layerGroup();
var E_Electric = L.layerGroup();

var depart_feu = L.layerGroup();
var feu_moyen = L.layerGroup();
var feu_gros = L.layerGroup();
var feu_fou = L.layerGroup();

var Itineraire = L.layerGroup();

var overlayMap_Type = {
    "A": A,
    "B_Gasoline": B_Gasoline,
    "B_Alcohol": B_Alcohol,
    "B_Plastics": B_Plastics,
    "C_Flammable_Gases": C_Flammable_Gases,
    "D_Metals": D_Metals,
    "E_Electric": E_Electric,
};

for (const [key, value] of Object.entries(overlayMap_Type)) {
    value.addTo(mymap);
}

var overlayMap_Intensity = {
    "fire start": depart_feu,
    "fire medium": feu_moyen,
    "fire big": feu_gros,
    "fire out of control": feu_fou
};

for (const [key, value] of Object.entries(overlayMap_Intensity)) {
    value.addTo(mymap);
}

var streets = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoidGhlbWRlaWwiLCJhIjoiY2twZTBuMXBzMXNzNjJubnhkdjU2YWVnaSJ9.JQOr5slR2vCCmEUEWNjI2A'
});

var darkmode = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/dark-v10',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoidGhlbWRlaWwiLCJhIjoiY2twZTBuMXBzMXNzNjJubnhkdjU2YWVnaSJ9.JQOr5slR2vCCmEUEWNjI2A'
});

darkmode.addTo(mymap);

var mapStyle = {
    "Normal": streets,
    "Dark": darkmode
}

var mapItineraire = {
    "Itineraires": Itineraire
}

L.control.layers(mapStyle, mapItineraire).addTo(mymap);
L.control.layers(null, overlayMap_Type).addTo(mymap);
L.control.layers(null, overlayMap_Intensity).addTo(mymap);

var FiretruckIcon = new L.Icon({
    iconUrl: '../media/firetruck.png',
    iconSize: [30, 30],
    iconAnchor: new L.Point(16, 16),
});
var CarIcon = new L.Icon({
    iconUrl: '../media/car.png',
    iconSize: [30, 30],
    iconAnchor: new L.Point(16, 16),
});
var PumperTruckIcon = new L.Icon({
    iconUrl: '../media/pumptruck.png',
    iconSize: [30, 30],
    iconAnchor: new L.Point(16, 16),
});
var WaterTenderIcon = new L.Icon({
    iconUrl: '../media/tendertruck.png',
    iconSize: [30, 30],
    iconAnchor: new L.Point(16, 16),
});
var LadderTruckIcon = new L.Icon({
    iconUrl: '../media/laddertruck.png',
    iconSize: [30, 30],
    iconAnchor: new L.Point(16, 16),
});
var TruckIcon = new L.Icon({
    iconUrl: '../media/truck.png',
    iconSize: [30, 30],
    iconAnchor: new L.Point(16, 16),
});

var CaserneIcon = new L.Icon({
    iconUrl: '../media/caserne.png',
    iconSize: [30, 30],
    iconAnchor: new L.Point(16, 16),
});





