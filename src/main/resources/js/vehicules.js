flagVehicle = false;
function displayInfoVehiculeMap() { //function goal is to gather all vehicules through a html request to the backend
    const context = {
        method: 'GET'
    }
    fetch('http://localhost:8082/Vehicule/vehicules', context) // sends a html request to our services  
        .then(response => response.json().then(body => displayVehicle(body)))
        .catch(error => console.log(error))
    setTimeout(displayInfoVehiculeMap, 1500);
}

function displayVehicle(body) {// function goal is to display vehicle's markers & delete old vehicle's markers from our page
    if (!flagVehicle) {
        mapIdVehicleLayerOld = new Map();
        flagVehicle = true;
    }
    flagIti = false;
    mapIdVehicleLayerNew = new Map();
    for (const vehicle of body) {
        if (!mapIdVehicleLayerOld.has(vehicle.id)) {

            var Vehicle = L.marker([vehicle.lat, vehicle.lon], {
                icon: FiretruckIcon
            }).addTo(mymap);
            Vehicle.bindPopup("Id : " + vehicle.id + "<br>Etat :" + vehicle.etat + "<br>Type : " + vehicle.type + "<br>Liquid Load : " + vehicle.liquidType + " " + vehicle.liquidQuantity + "L<br>" + "Fuel : " + vehicle.fuel + "<br><button type='button' id=" + vehicle.id + " onclick=deleteVehicle(this.id)>Supprimer</button><button id=" + vehicle.id + " type='button' onclick=editVehicle(this.id,"+vehicle.lat+","+vehicle.lon+","+vehicle.fuelConsumption+","+vehicle.fuel+")>Modifier</button>");
 
            if (vehicle.type == "CAR") {
                Vehicle.setIcon(CarIcon);
            }
            if (vehicle.type == "FIRE_ENGINE") {
                Vehicle.setIcon(FiretruckIcon);
            }
            if (vehicle.type == "PUMPER_TRUCK") {
                Vehicle.setIcon(PumperTruckIcon);
            }
            if (vehicle.type == "WATER_TENDERS") {
                Vehicle.setIcon(WaterTenderIcon);
            }
            if (vehicle.type == "TURNTABLE_LADDER_TRUCK") {
                Vehicle.setIcon(LadderTruckIcon);
            }
            if (vehicle.type == "TRUCK") {
                Vehicle.setIcon(TruckIcon);
            }
            mapIdVehicleLayerNew.set(vehicle.id,[vehicle.type,vehicle.liquidType,Vehicle,vehicle.facilityRefID,vehicle.liquidQuantity,vehicle.fuel,true])
        }  
        else{
            mapIdVehicleLayerOld.get(vehicle.id)[2].getPopup().setContent("Id : " + vehicle.id + "<br>Etat :" + vehicle.etat + "<br>Type : " + vehicle.type + "<br>Liquid Load : " + vehicle.liquidType + " " + vehicle.liquidQuantity + "L<br>" + "Fuel : " + vehicle.fuel + "<br><button type='button' id=" + vehicle.id + " onclick=deleteVehicle(this.id)>Supprimer</button><button id=" + vehicle.id + " type='button' onclick=editVehicle(this.id,"+vehicle.lat+","+vehicle.lon+","+vehicle.fuelConsumption+","+vehicle.fuel+")>Modifier</button>" + "<br><audio autoplay src='../media/Street-noise-sound-effect.mp3'>");
            mapIdVehicleLayerOld.get(vehicle.id)[2].setLatLng(L.latLng(vehicle.lat,vehicle.lon))
            mapIdVehicleLayerNew.set(vehicle.id,[vehicle.type,vehicle.liquidType,mapIdVehicleLayerOld.get(vehicle.id)[2],vehicle.facilityRefID,vehicle.liquidQuantity,vehicle.fuel,mapIdVehicleLayerOld.get(vehicle.id)[6]]);

            if (vehicle.type != mapIdVehicleLayerOld.get(vehicle.id)[0]){
                if (vehicle.type == "CAR") {
                    mapIdVehicleLayerOld.get(vehicle.id)[2].setIcon(CarIcon);
                }
                if (vehicle.type == "FIRE_ENGINE") {
                    mapIdVehicleLayerOld.get(vehicle.id)[2].setIcon(FiretruckIcon);
                }
                if (vehicle.type == "PUMPER_TRUCK") {
                    mapIdVehicleLayerOld.get(vehicle.id)[2].setIcon(PumperTruckIcon);
                }
                if (vehicle.type == "WATER_TENDERS") {
                    mapIdVehicleLayerOld.get(vehicle.id)[2].setIcon(WaterTenderIcon);
                }
                if (vehicle.type == "TURNTABLE_LADDER_TRUCK") {
                    mapIdVehicleLayerOld.get(vehicle.id)[2].setIcon(LadderTruckIcon);
                }
                if (vehicle.type == "TRUCK") {
                    mapIdVehicleLayerOld.get(vehicle.id)[2].setIcon(TruckIcon);
                }
            }
            mapIdVehicleLayerOld.delete(vehicle.id);
        }
        if ((vehicle.etat == 'DIRFEU' || vehicle.etat == 'DIRCASERNE')){
            if(!mapIti.has(vehicle.id)){
                displayJourneyReshaped(vehicle);
            }
            else if (mapIti.has(vehicle.id)){
                coordVehicule = new L.LatLng(vehicle.lat, vehicle.lon)
                if(!coordVehicule.equals(mapIti.get(vehicle.id)[2][mapIti.get(vehicle.id)[2].length - 1])){
                    mapIdVehicleLayerNew.get(vehicle.id)[6] = false;
                    if(mapIti.get(vehicle.id)[2].length > 2){
                        for(let i=0;i<mapIti.get(vehicle.id)[2].length-1;i++){
                            if(coordVehicule.distanceTo(mapIti.get(vehicle.id)[2][i]) < 150){
                                pointList = mapIti.get(vehicle.id)[2];
                                for(let j=0; j < i + 1; j++){
                                    pointList.shift();
                                }
                                mymap.removeLayer(mapIti.get(vehicle.id)[0]);
                                Itineraire.removeLayer(mapIti.get(vehicle.id)[0]);
                                var firstpolyline = new L.Polyline(pointList, {
                                    color: mapIti.get(vehicle.id)[1],
                                    weight: 3,
                                    opacity: 0.5,
                                    smoothFactor: 1
                                    });
                                firstpolyline.addTo(Itineraire);
                                color = mapIti.get(vehicle.id)[1];
                                if(mapIti.has(vehicle.id)){
                                    mapIti.set(vehicle.id,[firstpolyline,color,pointList]);
                                }
                                break;
                            }
                        }
                    }
                    mapIdVehicleLayerNew.get(vehicle.id)[6] = true;
                }
            }
        }
        if(vehicle.etat == 'ATFEU' || vehicle.etat == 'ATCASERNE' || vehicle.etat == 'DISPONIBLE'){
            if(mapIti.has(vehicle.id) && mapIdVehicleLayerNew.get(vehicle.id)[6]){
                mymap.removeLayer(mapIti.get(vehicle.id)[0]);//we remove the marker from the map
                Itineraire.removeLayer(mapIti.get(vehicle.id)[0]);
                mapIti.delete(vehicle.id);//we delete the journey from the old map
            }
        }
    }
   
    if (mapIdVehicleLayerOld.size != 0){
        mapIdVehicleLayerOld.forEach(function(value, key) {
            mymap.removeLayer(value[2]);//we remove the marker from the map
            mapIdVehicleLayerOld.delete(key);//we delete the vehicule from the old map
        });
    }
    mapIdVehicleLayerOld = new Map(mapIdVehicleLayerNew);      
}

function deleteVehicle(id) { //function goal is to delete a vehicle from our services 
    mymap.closePopup();
    if (mapIti.has(id)){
        mymap.removeLayer(mapIti.get(id)[0]);
        Itineraire.removeLayer(mapIti.get(id)[0]);
        mapIti.delete(id);
    }
    const context = {
        method: 'DELETE',
    }
    fetch('http://localhost:8082/Vehicule/delete/'+id, context)
        .then()
        .catch(error => console.log(error))
}

TruckTypeSelect = '<label for="TruckType">Choose a type of car :<br></label><select id="TruckType" name="TruckType"><option value="CAR">CAR</option><option value="WATER_TENDERS">WATER_TENDERS</option><option value="TURNTABLE_LADDER_TRUCK">TURNTABLE_LADDER_TRUCK</option><option value="TRUCK">TRUCK</option><option value="FIRE_ENGINE">FIRE_ENGINE</option><option value="PUMPER_TRUCK">PUMPER_TRUCK</option></select>';
LiquidTypeSelect = '<label for="LiquidType">Choose a type of liquid :<br></label><select id="LiquidType" name="LiquidType"><option value="ALL">ALL</option><option value="WATER">WATER</option><option value="WATER_WITH_ADDITIVES">WATER_WITH_ADDITIVES</option><option value="CARBON_DIOXIDE">CARBON_DIOXIDE</option><option value="POWDER">POWDER</option></select>';
CaserneNumber = '<label for="CaserneNumber">Choose a firehouse number(max 10 firehouses):<br></label><input type="number" id="Caserne" name="Caserne" min="1" max="10" value= "1" step="1">';
var AddPopup = L.popup().setContent('Define what vehicule you desire<br><form onsubmit=AddVehicle(event) method="POST" id="AddVehicle">' + TruckTypeSelect + '<br>' + LiquidTypeSelect + '<br>' + CaserneNumber + '<input type="submit"></form>');

AddButton = L.easyButton('<img title = "Add a vehicle" src="../media/addcar.png">', function (btn, map) {
    AddPopup.setLatLng(map.getCenter()).openOn(map);
}).addTo(mymap);

function AddVehicle(event) { // function goal is to add a new vehicle to our services 
    event.preventDefault();
    mymap.closePopup();
    var charge = {
        "lon": 4.828066,
        "lat": 45.747389,
        "type": document.getElementById('TruckType').value,
        "efficiency": 10.0,
        "liquidType": document.getElementById('LiquidType').value,
        "liquidQuantity": 100.0,
        "liquidConsumption": 1.0,
        "fuel": 100.0,
        "fuelConsumption": 1,
        "crewMember": 8,
        "crewMemberCapacity": 8,
        "facilityRefID": document.getElementById('Caserne').value,
        "id": 0
    }

    const context = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(charge)
    }
    fetch("http://localhost:8082/Vehicule/newVehicule", context)
        .then()
        .catch(error => console.log(error))

}

function editVehicle(id,lat,lon,fuelconso,fuel) { // function goal is to display a form popup to edit a specific vehicle already in our services

    TruckTypeSelect = '<label for="TruckTypeEdit">Choose a type of car :<br></label><select id="TruckTypeEdit" name="TruckTypeEdit"><option value="CAR">CAR</option><option value="WATER_TENDERS">WATER_TENDERS</option><option value="TURNTABLE_LADDER_TRUCK">TURNTABLE_LADDER_TRUCK</option><option value="TRUCK">TRUCK</option><option value="FIRE_ENGINE">FIRE_ENGINE</option><option value="PUMPER_TRUCK">PUMPER_TRUCK</option></select>';
    LiquidTypeSelect = '<label for="LiquidTypeEdit">Choose a type of liquid :<br></label><select id="LiquidTypeEdit" name="LiquidTypeEdit"><option value="ALL">ALL</option><option value="WATER">WATER</option><option value="WATER_WITH_ADDITIVES">WATER_WITH_ADDITIVES</option><option value="CARBON_DIOXIDE">CARBON_DIOXIDE</option><option value="POWDER">POWDER</option></select>';
    CaserneNumber = '<label for="CaserneNumber">Choose a firehouse number(max 10 firehouses):<br></label><input type="number" id="CaserneEdit" name="CaserneEdit" min="1" value = "1" max="10" step="1">';
    FuelEdit = '<label for="FuelEdit">Fuel:</label><br><input type="number" id="FuelEdit" name="FuelEdit" min="0" max="100" step = "0.00001" value='+fuel+'>';
    FuelConsoEdit = '<label for="FuelConsoEdit">Fuel Conso:</label><br><input type="number" id="FuelConsoEdit" name="FuelConsoEdit" min="0" max="10" step = "0.01" value='+fuelconso+'>';
    var EditPopup = L.popup().setContent('Define what vehicule you desire<br><form onsubmit=UpdateVehicle(event,' + id + ') method="POST" id="UpdateVehicle">' + TruckTypeSelect + '<br>' + LiquidTypeSelect + '<br>' + CaserneNumber +'<br>' +FuelEdit+ '<br>'+ FuelConsoEdit +'<br><label for="latEdit">Latitude (between 45.666 and 45.8373):</label><br><input type="number" id="latEdit" name="latEdit" min="45.666" max="45.8373" value = "'+lat+'" step="0.000000000000001"><br><label for="lonEdit">Longitude (between 4.688 and 4.97):</label><br><input type="number" id="lonEdit" name="lonEdit" min="4.688" max="4.97" value = "'+lon+'" step="0.000000000000001"><input type="submit"></form>');
    EditPopup.setLatLng(mymap.getCenter()).openOn(mymap);
}
function UpdateVehicle(event, id) { // function goal is to actually edit the vehicles of our services using the values of the precedent function's form
    event.preventDefault();
    mymap.closePopup();
    var charge = {
        "id": id,
        "lon": document.getElementById('lonEdit').value,
        "lat": document.getElementById('latEdit').value,
        "type": document.getElementById('TruckTypeEdit').value,
        "efficiency": 10.0,
        "liquidType": document.getElementById('LiquidTypeEdit').value,
        "liquidQuantity": 100.0,
        "liquidConsumption": 0.1,
        "fuel": document.getElementById('FuelEdit').value,
        "fuelConsumption": document.getElementById('FuelConsoEdit').value,
        "crewMember": 8,
        "crewMemberCapacity": 8,
        "facilityRefID": document.getElementById('CaserneEdit').value
    }
    const context = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(charge)
    }
    fetch("http://localhost:8082/Vehicule/modif/" + id, context)

}

displayInfoVehiculeMap(); // simple function call to launch the recurring update
