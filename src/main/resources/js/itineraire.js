mapIti = new Map();
function displayJourneyReshaped(vehicule) {
    id = vehicule.id
    if(mapIdVehicleLayerNew.get(id)[6]){
        const context = {
            method: 'GET'
        }
        fetch('http://localhost:8082/Deplacement/getIti/'+id, context)
            .then(response => response.json().then(body => displayIti(body,id)))
            .catch(error => console.log(error))
    }
}

function displayIti(body,id){
    var pointList = [];
    body.forEach(coord => {
        pointList.push(new L.LatLng(coord.y, coord.x));
    });
    
    colorIti = colorAlea();
    var firstpolyline = new L.Polyline(pointList, {
        color: colorIti,
        weight: 3,
        opacity: 0.5,
        smoothFactor: 1
    
        });
    firstpolyline.addTo(Itineraire);
    mapIti.set(id,[firstpolyline,colorIti,pointList]);
}

function colorAlea(){
    return ('#'+(Math.random()*0xFFFFFF<<0).toString(16))
}
